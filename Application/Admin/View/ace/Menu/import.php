<extend name="Public/base" />

<block name="body">
<!-- 表单 -->
<form id="form" action="{:U('import')}" method="post" class="form-horizontal">

	<div class="form-group">
		<label class="col-xs-12 col-sm-2 control-label no-padding-right">导入的内容<span class="check-tips">（请按照导入格式输入）</span></label>
		<div class="col-xs-12 col-sm-6">
			<textarea name="tree" class="form-control" rows="5"></textarea>
		</div>
		<div class="help-block col-xs-12 col-sm-reset inline">
			导入格式：
			<br>标题|url(回车键)
			<br>标题|url
		</div>
	</div>

	<?=ace_srbtn()?>

	<input type="hidden" name="pid" value="{$pid}"/>
	<input type="hidden" name="cate_id" value="{$cate_id|default=0}">
</form>
</block>

<block name="script">
<script type="text/javascript">

$('#submit').click(function(){
	$('#form').submit();
});


$(function(){
	//导航高亮
	highlight_subnav('{:U('index')}');
});
</script>
</block>
